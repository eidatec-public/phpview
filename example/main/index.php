<?php

require_once __DIR__."/../../phpview.php";
require_once __DIR__."/MainTemplate.php";

$view = new MainTemplate([
    
    //Übergabe von Strings
    'info-header'=>'Für PHP Fullstack-Entwickler',
    
    //Übergabe von komletten Objekten möglich
    'info'=>(new EdtHtmlTag())->attrClass('blockquote blockquote-success')
                ->p()
                    ->inner('Höchste Performance für höchste Ansprüche!')->back()
                ->ol()->attrClass('list-group')
                    ->li(['class'=>'list-group-item glyphicon glyphicon-ok'])
                        ->inner('Einfache Entwicklung')->back()
                    ->li(['class'=>'list-group-item glyphicon glyphicon-ok'])
                        ->inner('Einfache Wiederverwendung')->back()
                    ->li(['class'=>'list-group-item glyphicon glyphicon-ok'])
                        ->inner('Einfache Wartung')->back()
                    ->li(['class'=>'list-group-item glyphicon glyphicon-ok'])
                        ->inner('Glückliche Entwickler')
                
                //Objektzeiger zurücksetzen, damit das komplette Object übergeben wird
                ->ready()        
]);

        
    $view->val('sitename', "EidaTec PHPView Templating-Engine");
    $view->val('pagetitle', "EidaTec PHPView Templating-Engine");
    $view->val('pagedescription', "EidaTec PHPView Templating-Engine");
    $view->val('pagekeywords', "EidaTec PHPView Templating-Engine");
        
    $view->val('version', "v. beta 0.1");   

    $view->val('copyright', "Copyright © 2017 EidaTec GmbH.");      

$view->show();