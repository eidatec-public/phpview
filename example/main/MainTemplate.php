<?php

class MainTemplate extends EdtTemplateController{
        
    
    public function __construct($in = null){
        parent::__construct($in); 
    }
    
    
    public function preProcess(){
        
        /*
         * Übergabe-Parameter wird in das Template eingefügt
         */
        $this->val('info-header', $this->get('info-header'));   
        $this->val('info', $this->get('info'));        
        
    }
    

    public function createContent(){
        
        $this->out($this->newHtmlElement('html', ['lang'=>'de'])
                            ->inner($this->getHead())
                            ->inner($this->getBody())
                        ->ready());   
        
    }
    
    
    protected function getHead(){
        
        $head = $this->newHtmlElement('head');
        
            $head->createSingle('meta', ['charset'=>'utf-8']);
            $head->createSingle('meta', ['name'=>'viewport', "content"=>"width=device-width, initial-scale=1.0 maximum-scale=1.0"]);
            $head->createSingle('meta', ['name'=>'title', "content"=>$this->tmp('pagetitle')]);
            $head->createSingle('meta', ['name'=>'description', "content"=>$this->tmp('pagedescription')]);
            $head->createSingle('meta', ['name'=>'keywords', "content"=>$this->tmp('pagekeywords')]);
            $head->createSingle('meta', ['name'=>'robots', "content"=>'index,follow']);
            $head->createSingle('meta', ['name'=>'revisit-after', "content"=>'3 days']);
            $head->createSingle('meta', ['name'=>'robots', "content"=>'index,follow']);          
            
            
            
            
            $head->createSingle('link', [
                                            'rel'=>'stylesheet', 
                                            'href'=>'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css',
                                            'integrity'=>'sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u',
                                            'crossorigin'=>'anonymous'
                                        ]);
            
            $head->createSingle('link', [
                                            'rel'=>'stylesheet', 
                                            'href'=>'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css',
                                            'integrity'=>'sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp',
                                            'crossorigin'=>'anonymous'
                                        ]);              
              
            $head->createSingle('link', ['rel'=>'stylesheet', 'href'=>'https://cdnjs.cloudflare.com/ajax/libs/prism/1.6.0/themes/prism-okaidia.min.css']);   
            
            $head->script([
                            'src'=>'https://code.jquery.com/jquery-3.2.1.slim.min.js',
                            'integrity'=>'sha256-k2WSCIexGzOj3Euiig+TlR8gA0EmPjuc79OEeY5L45g=',
                            'crossorigin'=>'anonymous'
                        ]);
            
            $head->script([
                            'src'=>'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js',
                            'integrity'=>'sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa',
                            'crossorigin'=>'anonymous'
                        ]);
                        
            $head->script(['src'=>'https://cdnjs.cloudflare.com/ajax/libs/prism/1.6.0/prism.min.js']);
            $head->script(['src'=>'https://cdnjs.cloudflare.com/ajax/libs/prism/1.6.0/components/prism-php.js']);            
            
            $head->createSingle('link', ['rel'=>'shortcut icon', "href"=>$this->tmp('favicon')]);
            
            $head->create('title')->inner($this->tmp('pagetitle'));               
            
            $head->createSingle('meta', ['property'=>'og:site_name', "content"=>$this->tmp('sitename')]);
            $head->createSingle('meta', ['property'=>'og:locale', "content"=>"de_DE"]);
            $head->createSingle('meta', ['property'=>'og:title', "content"=>$this->tmp('pagetitle')]);
            $head->createSingle('meta', ['property'=>'og:type', "content"=>'website']);
            $head->createSingle('meta', ['property'=>'og:image', "content"=>$this->tmp('og_image')]);
            $head->createSingle('meta', ['property'=>'og:image:height', "content"=>$this->tmp('og_image_height')]);
            
            $head->createSingle('meta', ['property'=>'og:image:width', "content"=>$this->tmp('og_image_width')]);
            $head->createSingle('meta', ['property'=>'og:url', "content"=>$this->tmp('fullpath')]);
            $head->createSingle('meta', ['property'=>'og:description', "content"=>$this->tmp('pagedescription')]);
                                        
        return $head;
        
    }
    

    protected function getBody(){
        
        $wrapper = null;
                
        $body = $this->newHtmlElement('body')
                    ->div()->attrClass('col-xs-12 col-xs-offset-0 col-sm-10 col-sm-offset-1')
                
                        /*
                         * Extraktion des EdtHtmlTag Object aus der Laufenden Hierarchie
                         */
                        ->extract($wrapper)
                        ->back()
                    ->div()->attrStyle('text-align: center;')->attrClass('col-xs-12 col-xs-offset-0 col-sm-10 col-sm-offset-1')
                        ->inner($this->tmp('copyright'))        
                    ->ready();

            $wrapper->div()->attrClass('page-header')
                        ->h2()->inner($this->tmp('pagetitle'));
                    
                /*
                 * Extraktion des erzeugten EdtHtmlTag Object über die Rückgabe der Methode
                 */
                $jumbo = $wrapper->div(['class'=>'jumbotron container-fluid', 'style'=>'text-align: center;']);   

                    $jumbo->div()->attrStyle('float: left; font-size: 120px; padding: 20px 50px;')->attrClass('col-md-4 col-xs-12')
                            ->span()->attrClass('glyphicon glyphicon-star');

                    $jumbo->h1()
                            ->inner('PHP > HTML');

                    $jumbo->p()
                            ->inner('PHP Entwickeln ohne Medienbruch');
                    
                    $jumbo->span()->attrClass('label label-danger')
                            ->inner($this->tmp('version'));
                    
                $blockquote = $wrapper->blockquote();
                
                    $blockquote->h2()
                                ->inner($this->tmp('info-header'));
                    
                    $blockquote->inner($this->tmp('info'));
                                        
            $wrapper->inner($this->getCodeExample());
        
        return $body;
    }
    
    
    protected function getCodeExample(){
        
        $example = $this->newHtmlElement('div', ['class'=>'container-fluid']);
        
        $example->div()->attrClass('well')
                ->h4()
                    ->inner('1. Template Engine Laden')
                    ->back()
                ->pre()
                    ->code()->attrClass('language-php')
                    ->inner("require_once '/path/to/phpview/phpview.php';");
        
        $example->div()->attrClass('well')
                ->h4()
                    ->inner('2. Template erstellen und abstrakte Methoden überschreiben')
                    ->back()
                ->pre()
                    ->code()->attrClass('language-php')
                    ->inner("
class MeinTemplate extends EdtTemplateController{
        
    /**
     * Wird bei jedem laden des Templates aufgerufen. Hier kommet die 
     * Verarbeitungslogik für im Konstruktor übergebene Werte rein.
     */
    protected function preProcess(){
        
        //Todo
        
    }
    
    /**
     * Hier wird das Template generiert und für das rendern vorbereitet
     * 
     * Wird nur beim generieren und cachen des Templates aufgerufen
     */
    protected function createContent(){
    
        //EdtHtmlTag Objekt erzeugen
        \$div = \$this->newHtmlElement('div');
        
            //Platzhalter einfügen
            \$div->h2()->inner(\$this->tmp('inhalt');
            
        \$this->out(\$div);
    }

}");
        
        
        $example->div()->attrClass('well')
                ->h4()
                    ->inner('3. Template Verwenden')
                    ->back()
                ->pre()
                    ->code()->attrClass('language-php')
                    ->inner(" 
\$meinTemplate = new MeinTemplate();
    
//Werte für Platzhalter übergeben
\$meinTemplate->val('inhalt', 'Mein erstes Template');

//Rendern und Zurückgeben 
\$html = \$meinTemplate->render();
    
//ODER

//Rendern und ausgeben
\$meinTemplate->show();");
        
        $example->div()->attrClass('well')
                ->h4()
                    ->inner('Ergebnis')
                    ->back()
                ->pre()
                    ->code()->attrClass('language-html')
                    ->inner(htmlentities("
<div>
    <h2>Mein erstes Template</h2>
</div>"));
        
        return $example;
        
    }
    
}

