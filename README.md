v.0.1 beta  

--------------------------------------------------------------------------------

# EidaTec PHPView Templating Engine

Die EidaTec PHPView ist eine Templating Engine, die es ermöglicht Templates in 
reinem PHP zu entwickeln, ohne Medienbruch.



## Funktion

PHPView ist eine sehr flexible Engine, die keine Programmstruktur vorschreibt, 
und in jedem PHP basierten Projekt mühelos eingesetzt werden kann. Die erstellten 
Templates werden gerendert und im `cache/` Verzeichnis abgespeichert. Dieses Verzeichnis
benötigt eine Schreibbechtigung.


```php
<?php
    protected function createContent(){
     
        $body = $this->newHtmlElement('body')
                ->div()
                    ->header()
                        ->h2()->inner($this->tmp('headline'))->back()
                        ->p()
                            ->span()->attrClass('glyphicon glyphicon-ok')->back()
                            ->inner($this->tmp('subtitle'))->back()
                        ->back()
                    ->section()
                        ->h2()->inner($this->tmp('content-title'))->back()
                        ->div()->inner($this->tmp('lorem-ipsum'))->ready();
                        
        $this->out($body);
    }
?>
```


--------------------------------------------------------------------------------

## Beispiel

#### 1. Engine laden  


```php
<?php
    require_once '/path/to/phpview/phpview.php';
?>
```  

--------------------------------------------------------------------------------

#### 2. Template erstellen und abstrakte Methoden überschreiben  

1. Das neue Template muss vom EdtTemplateController erben und die abstrakten Methoden
überschreiben. Das Template wird in der Methode `createContent()` generiert und im Cache
gespeichert. Diese Methode wird nicht mehr aufgerufen, bis sich das Template ändert.
2. Felder die mit Inhalt befüllt werden müssen, werden über die Methode `$this->tmp($key)`
mit einem Platzhalter versehen.
3. Die Template-Struktur wird über `$this->out($html)` an die Engine
übergeben.
4. Falls es erforderlich ist Content dynamisch zu generieren, so kommt die Logik in die Methode
`preProcess()` rein. Diese Methode wird immer aufgerufen bevor das gecachte Template
gerendert wird. Die generierten Werte werden über `$this->val($key, $value)` an die Engine
übergeben und beim rendern übernommen.


```php
<?php
    class MeinTemplate extends EdtTemplateController{
        
        /**
         * Wird bei jedem laden des Templates aufgerufen. Hier kommet die 
         * Verarbeitungslogik für im Konstruktor übergebene Werte rein.
         */
        protected function preProcess(){
            
            /*
             *  Hier werden die im Konstruktor übergebenen Werte verarbeitet
             *
             *  Wert auslesen
             *  $wert = $this->get($key);
             *
             *  An die Engine übergeben
             *  $this->val($key, $wert);
             *
             */
            
        }
        
        /**
         * Hier wird das Template generiert und für das rendern vorbereitet
         * 
         * Wird nur beim generieren und cachen des Templates aufgerufen
         */
        protected function createContent(){
        
            //EdtHtmlTag Objekt erzeugen
            $div = $this->newHtmlElement('div');
            
                //Platzhalter einfügen
                $div->h2()->inner($this->tmp('inhalt');
                
            $this->out($div);
        }
    
    }
?>
```  

--------------------------------------------------------------------------------

#### 3. Template Verwenden  

1. Template instantiieren und über die Methode `->val($key, $value)` mit Werten füllen
2. Im Konstruktor kann ein `array` an Parametern übergeben werden. Diese Werte müssen dann
in der `->preProcess()` Methode verarbeitet und mit `->val($key, $value)` an die Engine übergeben werden.
2. Das Template kann entweder über `->render()` für die weitere Verarbeitung zwischengespeichert, 
oder mit `->show()` direkt ausgegeben werden.

```php
<?php
    $meinTemplate = new MeinTemplate();
    
    //Werte für Platzhalter übergeben
    $meinTemplate->val('inhalt', 'Mein erstes Template');

    //Rendern und Zurückgeben 
    $html = $meinTemplate->render();
    
    //ODER

    //Rendern und ausgeben
    $meinTemplate->show();
?>
```

--------------------------------------------------------------------------------

### 4. Ergebnis

```html
<div>
    <h2>Mein erstes Template</h2>
</div>
```

--------------------------------------------------------------------------------

# Der Html-Wrapper

Die Klasse `EdtHtmlTag` ist ein Html-Wrapper den die Template-Engine verwendet um den
Html Code zu generieren. Ein `EdtHtmlTag` Objekt repräsentiert immer genau ein Html Element, 
das Element, welches im Konstruktor übergeben wird.

## Funktion

--------------------------------------------------------------------------------

### Instantiierung

1. Instantiierung im Template `$this->newHtmlElement('div'); //oder ein beliebiges anderes Html Element`
2. Instantiierung über den `new` Operator `$div = new EdtHtmlTag($params, $parent, 'div');`
3. Instantiierung über ein vorhandenes Objekt `$h2 = $div->h2($params);`
    * Diese Art der Instantiierung bindet `$h2` automatisch als ein Unterelement von `$div`

--------------------------------------------------------------------------------

### Der Interne Zeiger

Einmal instantiiert, lässt sich das EdtHtmlTag Objekt beliebig oft replizieren und mit dem internen
Hierarchie-Zeiger, durch die Hierarchie navigieren. Jeder Methodenaufruf gibt das
gibt das Objekt, oder das neu instantiierte Objekt wieder zurück. So lässt sich eine 
Kette von Methodenaufrufen realisieren, ohne die Objekte in eine extra Variable zu schreiben.

```php
<?php
    
    $body = (new EdtHtmlTag(null, null, 'body')) //Element body wird instantiiert
                
                //Unterelement von body
                //Zeiger setzt sich auf das neu instantiierte div element
                ->div()
                    
                    //Unterelement von div
                    //Zeiger setzt sich auf h2
                    ->h2()
                        ->inner('Eine Überschrift')
                        
                        //Zeiger zurück zu div
                        ->back()
                        
                    ->p()
                        ->inner('Eine Zeile')
                        
                        /*  WICHTIG
                         *  In einer solchen Konstruktion, ist es wichtig den Zeiger 
                         *  auf das erste Element zurückzusetzen, damit es korrekt
                         *  in der Variable $body gespreichert werden kann. Dies geschieht 
                         *  mit der ->ready() Methode
                         */
                        ->ready();
                    
                    
?>
```

--------------------------------------------------------------------------------

### Single Tags

Alle Elemente die generiert werden, werden automatisch geschlossen. Tags die nicht
geschlossen werden sollen (z.B `img` oder `link`) werden wie Folgt instantiiert

```php
<?php
    
    //Der letze parameter bestimmt ob das Element geschlossen wird 
    $img = new EdtHtmlTag(null, null, 'img', FALSE)
    
    //Durch Erweiterung über die Methode ->createSingle($tag, $params)
    $div = (new EdtHtmlTag())->createSingle('img');
?>
```

--------------------------------------------------------------------------------

### Die Tag Attribute

Die Attribute für das Html Element werden auf drei arten übergeben, als assoziatives 
Array im Konstruktor, oder als string in der Methode

1. im Konstruktor `new EdtHtmlTag(['class'=>'meine-css-klasse'])`
2. beim erweitern `$div->h2(['class'=>'meine-css-klasse']);`
3. nach dem erweitern `$div->h2()->attrClass('meine-css-klasse');`
    * Die letze Methode kann auch auf `style` mit `->attrStyle('css string')` und `id` mit `->attrId('id')` angewendet werden
    
------------------------------------------------------------------------------------

### Objekte aus der Hierarchie entnehmen
    
Jedes Objekt kann jederzeit aus der Hierarchie in eine Variable geschrieben und an 
anderer Stelle verarbeitet werden.

```php
<?php

    /*
     * Variante 1
     */
    $body = (new EdtHtmlTag(null, null, 'body'))
                ->div()
                    ->h2()
                        ->inner('Eine Überschrift')
                        ->back()
                    ->p()
                    
                        /*
                         * Die Methode ->extract() bindet das aktuelle Objekt an $p
                         * ohne die Hierarchie zu unterbrechen
                         */
                        ->extract($p)
                        ->ready();
                        
    
                    $p->inner('Eine Zeile');
                    
                    
                    
     /*
     * Variante 2
     */
    $body = new EdtHtmlTag(null, null, 'body');
    
    
        //Aufruf von ->ready() nicht mehr nötig
        $body->div()
                    ->h2()
                        ->inner('Eine Überschrift');
                        
                        
            $p = $body->p();
        
            $p->inner('Eine Zeile');
        
?>
```

