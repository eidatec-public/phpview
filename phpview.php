<?php

ini_set('display_errors', 1);


/*
 * Template Caching aktivieren
 * 
 * Nur für produktiven Einsatz 
 */
if(!defined('edt_phpview_use_cache')){
    define('edt_phpview_use_cache', true);
}

/*
 * Template-Cache Ordner Pfad
 */
if(!defined('edt_phpview_cache_dir')){
    define('edt_phpview_cache_dir', __DIR__."/cache/");
}

/*
 *  Prüfen ob der Cache-Dir beschreibbar ist 
 */
if(edt_phpview_use_cache && !is_writable(edt_phpview_cache_dir)){
    throw new Exception("Fehler: Das Cache Verzeichnis ist nicht beschreibbar(".edt_phpview_cache_dir.")");
}

require_once __DIR__."/src/EdtHtmlTag.php";
require_once __DIR__."/src/EdtTemplateController.php";
