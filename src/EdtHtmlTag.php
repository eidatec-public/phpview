<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * HtmlTag Objekt
 *
 * @author eidatec
 */
class EdtHtmlTag{
    
    protected $tag;
    protected $closable;
    protected $params;
    protected $inner = [];
    protected $parent;

    public function __construct($params = null, EdtHtmlTag $parent = null, $tag = 'div', $closable = true){
        
        $this->params = $params;
        $this->parent = $parent;
        $this->tag = $tag;
        $this->closable = $closable;
        
        if($this->parent !== null){
            $this->parent->inner($this);
        }
        
    }
    
    /**
     * Erzeugt ein neues HtmlTag Objekt 
     * @return EdtHtmlTag
     */
    public function __call($name, $arguments){
        return $this->create($name, isset($arguments[0]) ? $arguments[0]:null);
    }

    /**
     * Erzeugt ein neues geschlossenes HtmlTag Objekt und bindet es als
     * Unterelement des aufrufenden HtmlTag Objekts 
     * 
     * @param string $tag Html-Tag Name
     * @param array $params Html-Tag Attribute als assoziatives Array
     * 
     * @return EdtHtmlTag Neues HtmlTag Objekt
     */
    public function create($tag, $params = null){
        return new EdtHtmlTag($params, $this, $tag, true);
    }
    

    /**
     * Erzeugt ein neues nicht geschlossenes HtmlTag Objekt und bindet es als
     * Unterelement des aufrufenden HtmlTag Objekts 
     * 
     * @param string $tag Html-Tag Name
     * @param array $params Html-Tag Attribute als assoziatives Array
     * 
     * @return EdtHtmlTag Neues HtmlTag Objekt
     */
    public function createSingle($tag, $params = null){
        return new EdtHtmlTag($params, $this, $tag, false);
    }
    
    
    /**
     * Setzt die ID des Html Elements, überschreibt eine vorhandene
     * 
     * @param string $id Neuer id-Tag des Elements
     * 
     * @return EdtHtmlTag Gibt sich selbt zurück
     */
    public function attrId($id){
        if(!$this->params) {$this->params = [];}
        
        $this->params['id'] = $id;        
        return $this;
    }
    
    
    
    
    /**
     * Extrahiert das Object in eine externe Variable für spätere Interaktion
     * 
     * @param string $id Neuer id-Tag des Elements
     * 
     * @return EdtHtmlTag Gibt sich selbt zurück
     */
    public function extract(&$object){
        $object = $this;
        return $this;
    }
    
    
    /**
     * Setzt den style-Tag des Html Elements, überschreibt ein vorhandenes
     * 
     * @param string $style Neuer style-Tag des Elements
     * 
     * @return EdtHtmlTag Gibt sich selbt zurück
     */
    public function attrStyle($style){
        if(!$this->params) {$this->params = [];}
        
        $this->params['style'] = $style;        
        return $this;
    }
    
    /**
     * Setzt den class-Tag des Html Elements, überschreibt ein vorhandenes
     * 
     * @param string $class Neuer class-Tag des Elements
     * 
     * @return EdtHtmlTag Gibt sich selbt zurück
     */
    public function attrClass($class){
        if(!$this->params) {$this->params = [];}
        
        $this->params['class'] = $class;        
        return $this;
    }


    /**
     * Setzt den Zeiger auf das Elternelement falls vorhanden
     * @return EdtHtmlTag Elternelement oder sich selbst
     */
    public function back(){
        if($this->parent !== null){
            return $this->parent;
        }else{
            return $this;
        }
    }
    
    /**
     * Rendert rekursiv alle Unterelemente und sich selbst
     * @return string Html String
     */
    public function ready(){
        return $this->parent !== null ? $this->parent->ready():$this;
    }
    
    /**
     * Inhalt des HtmlTag Elements
     * 
     * @return EdtHtmlTag Gibt sich selbt zurück
     */
    public function inner($html){
        $this->inner[] = $html;
        return $this;
    }

    
    /**
     * Rendert das HtmlTag Objekt zu einem String und Verpackt es in das
     * gewünschte Html Element
     * 
     * @return string Html String
     */
    public function renderIn($tag, $params = null){
        return "<".$tag.($params !== null ? self::getAttributesString($params):'').">\n".(count($this->inner) > 0 ? implode('', $this->inner):null)."</".$tag.">\n";
    }
    
    /**
     * Rendert das HtmlTag Objekt zu einem String
     * 
     * @return string Html String
     */
    public function render(){
        
        /*
         * Jedem img ein alt-Tag 
         */
        if($this->tag == 'img' || $this->tag == 'image'){
            if(!$this->params){
                $this->params = [];
            }
            if(!isset($this->params['alt'])){
                $this->params['alt'] = "Bild";
            }
        }
        
        return "<".$this->tag.($this->params !== null ? self::getAttributesString($this->params):'').($this->closable ? ">\n". implode('', $this->inner)."\n</".$this->tag.">\n":" />\n");
    }
    
    
    /**
     * Ruft automatisch die ->render() Methode auf
     * @return string Html String
     */
    public function __toString() {
        $ret = $this->render();
        if(!is_string($ret)){
            return "";
        }
        
        return $ret;
    }       
    
    /**
     * Erzeugt aus dem assoziativen Array eine Zeichenkette aus attributen für 
     * den Html Tag
     * 
     * @param array $params Assoziatives Array
     * @return string Attribute als Zeichenkette
     */
    protected static function getAttributesString($params){
                
        $attrString = '';
        if(is_array($params)){
            foreach ($params as $key => $value) {

                //Ignoriert nicht aktivierte Attribute
                if(($key == 'checked' || $key == 'selected' || $key == 'disabled')){
                    $attrString .= $value;
                    continue;
                }

                if($value !== null){
                    $attrString .= " ".$key.'="'.$value.'"';
                }else{
                    $attrString .= " ".$key;
                }
            }
        }
        
        return $attrString;
    }
}
