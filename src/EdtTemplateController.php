<?php


/**
 * Hauptcontroller für alle Templates
 */
abstract class EdtTemplateController {
    
    /**
     * Input für Interne Verarbeitung
     * 
     * Wird nicht automatisch in das Template übernommen
     * 
     * @var array  
     */
    private $_in = [];
    
    
    /**
     * Template Struktur
     * 
     * Alles was gerendert (und gecached) werden soll
     * 
     * @var array  
     */
    private $_out = [];
        
    
    /**
     * Template Inhalt
     * 
     * Wird während des rendern über den Schlüssel direkt ins Template übernommen
     * 
     * Wird nicht gecached
     * 
     * @var array  
     */
    private $_vals = [];
    
    
    /**
     * Cache aktivieren oder deaktivieren
     * 
     * Kann im Erbenden Template eingestellt werden
     * @var bool
     */
    protected $use_cache = edt_phpview_use_cache;
    
    /**
     * Sekunden nach denen das Template verfällt und neu gerendert wird
     * 
     * Kann im Erbenden Template eingestellt werden
     * 0 = Nie
     * @var int
     */
    protected $cacheexpire = 0;
    
    /**
     * Schlüssel für das Templatecache Verzeichnis
     * @var string 
     */
    private $cachekey = null;
    
    /**
     * Templatecache Verzeichnis
     * @var string 
     */
    private $cachedir = null;
    
    /**
     * Temporärer Name der Cache-Datei
     * 
     * wird automatisch bestimmt über das letze Änderungsdatum des aufrufenden
     * Templates 
     * 
     * @var string 
     */
    private $templatetimestamp = null;
    
    /**
     * Gerendertes Template als String
     * @var string 
     */
    private $cachedTemplate = null;

    
    /**
     * @param array $in Input für Weitere interne Verarbeitung im Template 
     */
    public function __construct($in = null){
        $this->_in = is_array($in) ? $in:[];       
        
        $this->cachekey = strtolower(trim(get_class($this)));
        $this->templatetimestamp = filemtime((new ReflectionClass(get_class($this)))->getFileName());
    }
    
    
    /**
     * Erzeugt einen Platzhalter für $key. Dieser wird beim aktiven Cache, durch
     * den entsprechenden Wert ersetzt
     * 
     * @param string $key Schlüssel für den Platzhalter
     * @return string Cache Platzhalter 
     */
    protected function tmp($key){
        return $this->use_cache ? "[t{$key}t]":(isset($this->_vals[$key]) ? $this->_vals[$key]:'');
    }

    /**
     * Setzt den Wert für Schlüssel $key der dann beim rendern in das Template 
     * übernommen wird
     * 
     * @param string $key Cache Schlüssel
     * @param mixed $val Wert
     */
    public function val($key, $val){        
        $this->_vals[$key] = $val;
    }  

    
    /**
     * Setzt den Wert für Schlüssel $key der dann beim rendern in der ->preProcess()
     * Methode verarbeitet werden kann.
     * 
     * Wird nicht automatisch in das Template übernommen
     * 
     * @param string $key Schlüssel
     * @param mixed $val Wert
     */
    protected function set($key, $val){
        $this->_in[$key] = $val;
    }       
    
    
    /**
     * Gibt den Wert für $key aus
     * 
     * @param string $key
     * @return mixed Wert oder null
     */
    protected function get($key){
        return isset($this->_in[$key]) ? $this->_in[$key]:null;
    }
    
    
    /**
     * Ausgabe zum Rendern
     * 
     * Wird bei aktiven Cache gecached
     * 
     * @param mixed $html Html oder Objekt
     */
    protected function out($html){
        $this->_out[] = $html;
    }
    
    
    /**
     * Rendert das Template für die Ausgabe
     * 
     * @return string Fertiges Template für die Ausgabe
     */
    public function render(){
        
        //Löschen falls bereits befüllt
        $this->cachedTemplate = '';
        
        //PreProzess für nicht cachbare inhalte und interne Template-Logik
        $this->preProcess();
        
        
        if($this->use_cache && $this->loadCached()){
            
            //Falls ein gecachtes Template existiert und geladen wurde, werte setzen
            return $this->putValues($this->cachedTemplate);
        }
        
        //Keine Cache-Datei vorhanden oder Cache nicht aktiv
        return $this->renderCached();
    }
    
    
    /**
     * Für das Template aus und erzeugt aus $_out einen String
     * 
     * Falls Cache aktiv, wird der Inhalt in die Cache-Datei geschrieben
     * 
     * @return string Fertiges Template für die Ausgabe
     */
    private function renderCached(){
        
        //Template Ausführen
        $this->createContent();
        
        //Rendern
        $this->cachedTemplate = implode('', $this->_out);
        
        //Falls cache deaktiviert, zurückgeben
        if(!$this->use_cache || $this->cachedTemplate == ''){
            return $this->cachedTemplate;
        }
        
        //Cachen falls schlüssel vorhanden
        if($this->cachekey !== null && strlen($this->templatetimestamp) > 0){
            $this->cachedir = edt_phpview_cache_dir.$this->cachekey;
        
            if(!is_dir($this->cachedir)){
                
                //Templatecache Verzeichnis erzeugen
                mkdir($this->cachedir);                
            }else{
                
                //Templatecache Verzeichnis leeren damit keine alten Cache-Dateien übrig bleiben
                //Passiert wenn das Template geändert wurde und sich dadurch der $templatetimestamp ändert
                $cachedFiles = glob($this->cachedir.'/*');
                if(is_array($cachedFiles) && count($cachedFiles) > 0){
                    foreach($cachedFiles as $cachedFile){
                        if(is_file($cachedFile)){
                            unlink($cachedFile); 
                        }
                    }
                }
            }
            
            //Cache-Datei erzeugen
            file_put_contents($this->cachedir."/".$this->templatetimestamp.".cache", $this->cachedTemplate);            
        }
        
        //Werte in das Template schreiben und zurückgeben
        return $this->putValues($this->cachedTemplate);
    }

    
    /**
     * Ersetzt die Platzhalter im Template durch die entsprechenden Werte
     * 
     * Platzhalter ohne Werte werden gelöscht
     * 
     * @param string $templateString Template
     * @return string Fertiges Template für Ausgabe
     */
    protected function putValues($templateString){
        
        $matches;
        
        //Platzhalter finden
        preg_match_all("/\[t([^\]]*)t\]/", $templateString, $matches);        
        if(is_array($matches) && count($matches[1]) > 0){
            
            $pattern = [];
            $replacement = [];
            
            //Werte für Ersetzung aufbereiten
            foreach($matches[0] as $index=>$tmpKey){
                $pattern[] = $tmpKey;
                $replacement[] = isset($this->_vals[$matches[1][$index]]) ? $this->_vals[$matches[1][$index]]:'';
            }
        
            //Platzhalter ersetzen
            $templateString = str_replace($pattern, $replacement, $templateString);
        }        
        
        return $templateString;
    }


    
    /**
     * Versucht das Gecachte Template zu laden, falls vorhanden.
     * 
     * Falls Ablaufzeit erreicht, wird der Ladeprozess abgebrochen. 
     * 
     * @return bool Erfolgreich geladen oder nicht
     */
    private function loadCached(){
        
        $this->cachedir = edt_phpview_cache_dir.$this->cachekey;
        $cacheFilePath = $this->cachedir."/".$this->templatetimestamp.".cache";
        
        if(is_file($cacheFilePath)){
            
            //Template abgelaufen, laden abbrechen
            if($this->cacheexpire > 0 && filemtime($cacheFilePath) < time()-$this->cacheexpire){                
                return false;
            }
                        
            $this->cachedTemplate = file_get_contents($cacheFilePath);
            
            //Falls Verfallsdatum, Datei anfassen und ablaufzeit zurücksetzen
            if($this->cacheexpire > 0){
                touch($cacheFilePath);
            }
            return true;
        }
        
        return false;
    }
    

    /**
     * Erzeugt ein neues geschlossenes HtmlTag Objekt 
     *     
     * @param string $tag Html-Tag Name
     * @param array $params Html-Tag Attribute als assoziatives Array
     * @param bool $closable Geschlossenes Element
     * 
     * @return EdtHtmlTag Neues HtmlTag Objekt
     */    
    protected function newHtmlElement($tag = 'div', $params = null, $closable = true){
        return new EdtHtmlTag($params, null, $tag, $closable);
    }


    /**
     * Ruft die ->render() Methode und gibt das Ergebnis zurück für weitere 
     * Verarbeitung.
     * 
     * @return string Fertiges Template für die Ausgabe
     */
    public function __toString(){
        return $this->render();
    }
    
    

    /**
     * Ruft die ->render() Methode und gibt das Ergebnis 
     * per echo aus
     */
    public function show(){
        echo $this->render();
    }

    
    /**
     * Wird bei jedem laden des Templates aufgerufen. Hier kommet die 
     * Verarbeitungslogik für im Konstruktor übergebene Werte rein.
     * 
     * Muss im erbenden Template überschrieben werden
     */
    abstract protected function preProcess();
    
    
    /**
     * Hier wird das Template generiert und für das rendern vorbereitet
     * 
     * Muss im erbenden Template überschrieben werden
     */
    abstract protected function createContent();
}